package com.wcpdoc.exam.core.entity;

public interface LoginUser {

	public Integer getId();

	public String getName();

	public String getLoginName();
}
